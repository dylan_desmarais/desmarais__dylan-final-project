import React from 'react'
import { Container } from 'reactstrap'

const Footer = () => {
    return(
    <footer className="footer">
        <Container>
            <p className="m-0 text-center text-white">Copyright &copy; Dylan Desmarais 2021</p>
        </Container>
    </footer>
  )
}

export default Footer