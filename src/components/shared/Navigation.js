import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'


const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    return (
        <Navbar className="nav_bar" expand="md" fixed="top">
            <Container>
            <NavbarBrand href=""><p className="title">Full Stack Web Developer</p></NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav>
                    <NavItem className="nav_items">
                        <NavLink tag={RouteLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem className="nav_items">
                        <NavLink tag={RouteLink} to="/resume">Resume</NavLink>
                    </NavItem>
                    <NavItem className="nav_items">
                        <NavLink tag={RouteLink} to="/portfolio">Portfolio</NavLink>
                    </NavItem>
                    <NavItem className="nav_items">
                       <NavLink tag={RouteLink} to="/contact">Contact</NavLink>
                    </NavItem>
                    <NavItem className="nav_items">
                       <NavLink tag={RouteLink} to="/quotes">Quotes</NavLink>
                    </NavItem>
                    <NavItem className="nav_items">
                        <NavLink tag={RouteLink} to="/submissions">Login</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
            </Container>
        </Navbar>
    )
}

export default Navigation