import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap'


const Resume = () => {
    return (
        <Container>
            <Row>
                <Col>
                    <img className="image_resume" src="http://placehold.it/900x600" alt="logo" />
                </Col>
                <Col>
                    <h1>About Me</h1>
                    <p>
                        A curious mind and natural problem solver, I thoroughly enjoy learning and take any opportunity to expand my knowledge and expertise.
                        This allows me to develop new ideas to optimize the efficiency and productivity of various business processes.
                        My extensive experience in different industries has allowed me to adapt to new situations, while continuously delivering superior customer service.
                    </p>
                    <Button className="contact_button" href="/contact">Contact Me</Button>
                </Col>
            </Row>
            <Row>
                <ul className="resume_location">
                    <li className="title"><b>Dylan Desmarais</b></li>
                    <li>Mississauga, ON L5n 5Y6</li>
                    <li>dylan-desmarais@live.ca</li>
                    <li>705.816.5690</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>WORK EXPERIENCE</b></p>
            </Row>
            <Row>
                <ul className="resume_location">
                    <li><b>Forklift Operator / Material Handler</b></li>
                    <li>Trillium Supply Chain - <i>Caledon, ON</i></li>
                    <li>March 2021 to June 2021</li>
                </ul>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>Provide exeptional inbound and outbound customer service calls regarding prescriptions.</li>
                    <li>Accurately identify and document perscription errors and follow company protocol for resolution.</li>
                    <li>Data entry, creating new patient and doctor profiles, archiving completed prescriptions.</li>
                    <li>Verify prescription accuracy and order details.</li>
                    <li>Kroll prescription software.</li>
                    <li>Processing client transactions.</li>
                    <li>Various office and administrateive duties.</li>
                    <li>Assist in prescription dispensing department as needed.</li>
                    <li>Assist in shipping and packaging department as needed.</li>
                </ul>
            </Row>
            <Row>
                <ul className="resume_location">
                    <li><b>Bilingual Office Assistant</b></li>
                    <li>Summit Pharmacy - <i>Aurora, ON</i></li>
                    <li>April 2020 to September 2020</li>
                </ul>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>Provide exeptional inbound and outbound customer service calls regarding prescriptions.</li>
                    <li>Accurately identify and document perscription errors and follow company protocol for resolution.</li>
                    <li>Data entry, creating new patient and doctor profiles, archiving completed prescriptions.</li>
                    <li>Verify prescription accuracy and order details.</li>
                    <li>Kroll prescription software.</li>
                    <li>Processing client transactions.</li>
                    <li>Various office and administrateive duties.</li>
                    <li>Assist in prescription dispensing department as needed.</li>
                    <li>Assist in shipping and packaging department as needed.</li>
                </ul>
            </Row>
            <Row>
                <ul className="resume_location">
                    <li><b>Bilingual Office Assistant</b></li>
                    <li>Summit Pharmacy - <i>Aurora, ON</i></li>
                    <li>May 2019 to August 2019</li>
                </ul>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>Provide exeptional inbound and outbound customer service calls regarding prescriptions.</li>
                    <li>Accurately identify and document perscription errors and follow company protocol for resolution.</li>
                    <li>Data entry, creating new patient and doctor profiles, archiving completed prescriptions.</li>
                    <li>Verify prescription accuracy and order details.</li>
                    <li>Kroll prescription software.</li>
                    <li>Processing client transactions.</li>
                    <li>Various office and administrateive duties.</li>
                    <li>Assist in prescription dispensing department as needed.</li>
                    <li>Assist in shipping and packaging department as needed.</li>
                </ul>
            </Row>
            <Row>
                <ul className="resume_location">
                    <li><b>Real Estate Sales Representative</b></li>
                    <li>Team Beauchesne | RE-MAX Chay Realty Brkg - <i>Barrie, ON</i></li>
                    <li>September 2017 to May 2018</li>
                </ul>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>Responsible for day-to-day operations.</li>
                    <li>Deliver exceptinal customer service for my clients in order to create an enjoyable and memorable experience.</li>
                    <li>Managed marketing and advertising campaigns through social media, email marketing, micro farming, community service, creating relationsips with local businesses, and face-to-face contact.</li>
                    <li>Demonstrate extensive industry and market knowledge.</li>
                    <li>Regular use of residential OREA forms for purchase and sale and of real estate.</li>
                    <li>Document reveneues and expenses.</li>
                    <li>Track and folow up with leads and potential buyers.</li>
                    <li>Familar with command real estate softwares such as Matrix, Stratus, DocuSign, etc...</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>EDUCATION</b></p>
            </Row>
            <Row>
                <ul className="education">
                    <li><b><i>Bacherlor's Degree in Business Administration (Deferred)</i></b></li>
                </ul>
            </Row>
            <Row>
                <ul className="education_location">
                    <li>Sheridan College - Mississauga, ON</li>
                    <li>September 2019 to April 2020</li>
                </ul>
            </Row>
            <Row>
                <ul className="education">
                    <li><b><i>Resl Estate Licence in Real Estate</i></b></li>
                </ul>
            </Row>
            <Row>
                <ul className="education_location">
                    <li>Ontario Real Estate College - Toronto, ON</li>
                    <li>March 2016 to August 2017</li>
                </ul>
            </Row>
            <Row>
                <ul className="education">
                    <li><b><i>Secondary School Diploma | Academic Course</i></b></li>
                </ul>
            </Row>
            <Row>
                <ul className="education_location">
                    <li>Ecole Secondaire Catholique Algonquin - North Bay, ON</li>
                    <li>September 2010 to June 2014</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>SKILLS</b></p>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>Responsible for day-to-day operations.</li>
                    <li>Deliver exceptinal customer service for my clients in order to create an enjoyable and memorable experience.</li>
                    <li>Managed marketing and advertising campaigns through social media, email marketing, micro farming, community service, creating relationsips with local businesses, and face-to-face contact.</li>
                    <li>Demonstrate extensive industry and market knowledge.</li>
                    <li>Regular use of residential OREA forms for purchase and sale and of real estate.</li>
                    <li>Document reveneues and expenses.</li>
                    <li>Track and folow up with leads and potential buyers.</li>
                    <li>Familar with command real estate softwares such as Matrix, Stratus, DocuSign, etc...</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>LANGUAGES</b></p>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li>English - Fluent/Expert Level</li>
                    <li>French - Fluent/Expert Level</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>CERTIFICATION/LICENCES</b></p>
            </Row>
            <Row>
                <ul className="education">
                    <li><b>Level G Drivers License</b></li>
                </ul>
            </Row>
            <Row>
                <ul className="education_location">
                    <li>July 2014 to Present</li>
                </ul>
            </Row>
            <Row className="resume_category">
                <p><b>REFERENCES</b></p>
            </Row>
            <Row className="resume_description">
                <ul>
                    <li><i>Available upon request.</i></li>
                </ul>
            </Row>
        </Container>
    )
}

export default Resume