import React from 'react'
import { Container, Row, Col, Button, CardBody, CardText, Card } from 'reactstrap'

const Home = () => {
    return(
        <Container>
            <Row>
                <Col>
                    <h1>Dylan Desmarais</h1>
                    <p>
                        Hello, my name is Dylan, as a full stack web developer I specialize in front-end as well as back-end development.
                        Interested in our services, contact us today!
                    </p>
                    <Button className="contact_button" href="/contact">Contact Me</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <img className="image_home" src="http://placehold.it/900x600" alt="" />
                </Col>
            </Row>
            <Card className="quote_home">
                <CardBody>
                    <CardText><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Row>
                <Col>
                    <Button className="quote_button" href="/quotes">My Favorite Quotes</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default Home