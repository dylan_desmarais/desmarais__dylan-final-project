import React from 'react'
import { Container, Card, CardBody, CardText } from 'reactstrap'

const Quotes = () => {
    return(
        <Container className="container">
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna, aliqua consectetur adipiscing elit.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, aliqua consectetur adipiscing elit.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, aliqua consectetur adipiscing elit.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
            <Card className="quote_page_bottom">
                <CardBody>
                    <CardText><p className="">"<i>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, aliqua consectetur adipiscing elit.</i>" - by unknown</p></CardText>
                </CardBody>
            </Card>
        </Container>
    )
}

export default Quotes