import React from 'react'
import { Container, Row, Col, Button, CardBody, CardTitle, CardFooter, Card } from 'reactstrap'

const Portfolio = () => {
    return(
        <Container>
            <Row className="top_card">
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 1</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 2</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 3</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 4</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 5</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 6</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 7</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 8</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 9</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
            <Row className="bottom_card">
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 10</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 11</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col>
                    <Card className="portfolio_card">
                        <CardBody>
                            <CardTitle><h2>Project 12</h2></CardTitle>
                        </CardBody>
                        <CardFooter>
                            <Button className="explore_button">Explore</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Portfolio